package com.saifulshahril.forum.services;

import com.saifulshahril.forum.services.fakers.UserFakerService;
import com.saifulshahril.forum.services.seeders.UserSeederService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import({UserSeederService.class, UserService.class, UserFakerService.class})
public class UserSeederServiceTest {
    @Autowired
    private UserSeederService userSeederService;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_should_persist_10_user_records_in_database() {
        assertEquals(0, userService.count());
        userSeederService.run();
        assertEquals(10, userService.count());
    }
}