package com.saifulshahril.forum.services;

import com.saifulshahril.forum.entities.User;
import com.saifulshahril.forum.services.fakers.UserFakerService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(UserFakerService.class)
public class UserFakerServiceTest {
    @Autowired
    private UserFakerService userFakerService;

    @Autowired
    private TestEntityManager testEntityManager;

    private User user;

    @Before
    public void setUp() throws Exception {
        user = userFakerService.make();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_should_not_persist_a_freshly_maked_object() {
        assertNull(user.getId());
    }

    @Test
    public void it_can_make_a_new_user() {
        assertNotNull(user);
    }

    @Test
    public void it_can_make_a_user_that_can_be_persisted_immediately() {
        testEntityManager.persist(user);
        assertNotNull(user.getId());
    }
}