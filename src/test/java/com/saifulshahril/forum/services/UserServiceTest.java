package com.saifulshahril.forum.services;

import com.saifulshahril.forum.entities.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(UserService.class)
public class UserServiceTest {
    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        System.err.println("There are " + userService.count() + " users before");
    }

    @After
    public void tearDown() {
        System.err.println("There are " + userService.count() + " users after");
    }

    @Test
    public void it_can_create_new_user() {
        User user = new User();
        user.setUsername("xxx");
        user.setFirstName("xxx");
        user.setLastName("xxx");

        assertNull(user.getId());
        userService.create(user);
        assertNotNull(user.getId());
    }

    @Test
    public void it_can_update_new_user() {
        User user = new User();
        user.setUsername("xxx");
        user.setFirstName("xxx");
        user.setLastName("yyy");

        assertNull(user.getId());
        userService.create(user);
        assertNotNull(user.getId());

        user.setUsername("zzz");
        user.setFirstName("zzz");
        user.setLastName("zzz");
        userService.update(user);
        assertEquals("zzz", user.getUsername());
        assertEquals("zzz", user.getFirstName());
        assertEquals("zzz", user.getLastName());
    }
}