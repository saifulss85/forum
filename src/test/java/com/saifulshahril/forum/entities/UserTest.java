package com.saifulshahril.forum.entities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserTest {
    @Autowired
    TestEntityManager testEntityManager;
    private User user;
    private Post post;
    private Post post2;
    private Set<Post> posts;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setUsername("xxx");
        user.setFirstName("xxx");
        user.setLastName("xxx");

        post = new Post();
        post.setBody("xxx");
        post.setTitle("xxx");

        post2 = new Post();
        post2.setBody("yyy");
        post2.setTitle("yyy");

        posts = new HashSet<>();
        posts.add(post);
        posts.add(post2);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_should_throw_when_persisting_when_username_is_null() {
        user.setUsername(null);
        testEntityManager.persist(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_should_throw_when_persisting_when_username_is_blank() {
        user.setUsername("");
        testEntityManager.persist(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_should_throw_when_persisting_when_first_name_is_blank() {
        user.setFirstName("");
        testEntityManager.persist(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_should_throw_when_persisting_when_first_name_is_more_than_50_chars() {
        user.setFirstName("01234567890123456789012345678901234567890123456789x");
        testEntityManager.persist(user);
    }

    @Test(expected = ConstraintViolationException.class)
    public void it_should_throw_when_persisting_when_last_name_is_more_than_50_chars() {
        user.setLastName("01234567890123456789012345678901234567890123456789x");
        testEntityManager.persist(user);
    }

    @Test
    public void it_can_persist_a_user_with_unpersisted_posts() {
        // given a user that hasn't yet been persisted

        // when we new up 2 posts and set them to belong to the user and persist the user
        post.setAuthor(user);
        post2.setAuthor(user);

        user.setPosts(posts);
        assertNotNull(user.getPosts());

        assertNull(user.getId());
        testEntityManager.persist(user);
        assertNotNull(user.getId());

        // then we expect the 2 posts to exist in the database
        Post fetchedPost = testEntityManager.find(Post.class, post.getId());
        Post fetchedPost2 = testEntityManager.find(Post.class, post2.getId());
        assertNotNull(fetchedPost);
        assertNotNull(fetchedPost2);
    }

    @Test
    public void it_can_persist_new_post_when_it_already_has_2_persisted_posts() {
        // given a user that has already been persisted, which already has 2 posts which have also been persisted
        post.setAuthor(user);
        post2.setAuthor(user);
        user.setPosts(posts);
        testEntityManager.persist(user);
        assertNotNull(user.getId());
        assertNotNull(post.getId());
        assertNotNull(post2.getId());

        // when we new up 1 post and set them to belong to the user
        Post newPost = new Post(null, "zzz", "zzz", user, null);
        user.getPosts().add(newPost);
        testEntityManager.flush();

        // then we expect the re-fetched user's posts to be size 3
        User fetchedUser = testEntityManager.find(User.class, user.getId());
        assertEquals(3, fetchedUser.getPosts().size());
    }
}