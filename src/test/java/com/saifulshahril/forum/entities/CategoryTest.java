package com.saifulshahril.forum.entities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CategoryTest extends BaseJpaTest {
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private Environment environment;

    private Category category;

    @Before
    public void setUp() throws Exception {
        category = new Category();
        category.setTitle("xxx");
    }

    @After
    public void tearDown() throws Exception {
        category = null;
    }

    @Test
    public void it_can_persist_a_new_category() {
        System.err.println(Arrays.toString(environment.getActiveProfiles()));
        assertNull(category.getId());
        testEntityManager.persist(category);
        assertNotNull(category.getId());
    }
}