package com.saifulshahril.forum.entities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
//@Import({DatabaseSeederService.class})
public class CategoryPostTest {
    @Autowired
    private TestEntityManager testEntityManager;
    private Category category;
    private Category category2;
    private Category category3;
    private Post post;

    @Before
    public void setUp() throws Exception {
        category = new Category(null, "1", new ArrayList<>());
        category2 = new Category(null, "2", new ArrayList<>());
        category3 = new Category(null, "3", new ArrayList<>());

        testEntityManager.persist(category);
        testEntityManager.persist(category2);
        testEntityManager.persist(category3);

        User author = new User(
                null,
                "username",
                "firstName",
                "lastName",
                new HashSet<>()
        );
        testEntityManager.persist(author);

        post = new Post(null, "xxx", "xxx", author, new ArrayList<>());
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_persist_a_new_post_and_assign_2_existing_categories_to_it() {
        // given 2 existing categories and a new post
        Category fetchedCategory = testEntityManager.find(Category.class, category.getId());
        assertNotNull(fetchedCategory);
        Category fetchedCategory2 = testEntityManager.find(Category.class, category2.getId());
        assertNotNull(fetchedCategory2);

        // when we assign the 2 categories to the post, persist the post and re-fetch that post from persistence
        post.addCategory(fetchedCategory);
        post.addCategory(fetchedCategory2);
        assertNull(post.getId());
        testEntityManager.persist(post);
        Post fetchedPost = testEntityManager.find(Post.class, post.getId());
        assertNotNull(fetchedPost.getId());

        // then we expect the post to exist and to have those 2 categories as well
        List<CategoryPost> categoryPosts = fetchedPost.getCategoryPosts();
        assertEquals(2, categoryPosts.size());
        assertTrue(categoryPosts.get(0).getCategory().getTitle().equalsIgnoreCase(fetchedCategory.getTitle()));
        assertTrue(categoryPosts.get(1).getCategory().getTitle().equalsIgnoreCase(fetchedCategory2.getTitle()));
    }

    @Test
    public void it_can_add_categories_and_auto_persist_without_needing_explicit_persist_after_adding() {
        // given a post that is already persisted and thus managed
        assertNull(post.getId());
        testEntityManager.persist(post);
        assertNotNull(post.getId());

        // when we add pre-persisted categories to that post and flush-clear
        post.addCategory(category);
        post.addCategory(category2);
        post.addCategory(category3);
        testEntityManager.flush();
        testEntityManager.clear();
        Long id = post.getId();
        post = null;

        // then the post, when re-fetched, should have have those 3 categories
        // without needing us to explicitly persist the post again after adding
        Post fetchedPost = testEntityManager.find(Post.class, id);
        testEntityManager.refresh(fetchedPost);
        assertEquals(3, fetchedPost.getCategoryPosts().size());
    }
}