package com.saifulshahril.forum.entities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PostTest {
    @Autowired
    private TestEntityManager testEntityManager;
    private Post post;
    private User author;

    @Before
    public void setUp() throws Exception {
        post = new Post();
        post.setBody("xxx");
        post.setTitle("xxx");

        author = new User();
        author.setUsername("yyy");
        author.setFirstName("yyy");
        author.setLastName("yyy");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void it_can_persist_a_new_post() {
        post.setAuthor(author);
        testEntityManager.persist(author);  // notice that we're persisting the author _after_ we set it as the post's author

        assertNull(post.getId());
        testEntityManager.persist(post);
        assertNotNull(post.getId());
    }

    @Test
    public void it_can_update_fields_without_needing_explicit_persist() {
        post.setAuthor(author);
        testEntityManager.persist(author);  // notice that we're persisting the author _after_ we set it as the post's author

        assertNull(post.getId());
        testEntityManager.persist(post);
        assertNotNull(post.getId());

        post.setTitle("zzz");
        post.setBody("zzz");

        Post fetchedPost = testEntityManager.find(Post.class, post.getId());
        assertEquals(fetchedPost.getTitle(), "zzz");
        assertEquals(fetchedPost.getBody(), "zzz");
    }

    @Test
    public void it_can_fetch_its_author() {
        assertNull(author.getId());
        testEntityManager.persist(author);
        assertNotNull(author.getId());  // to confirm the author has been persisted

        post.setAuthor(author);

        assertNull(post.getId());
        testEntityManager.persist(post);
        assertNotNull(post.getId());    // to confirm the post has been persisted

        Post fetchedPost = testEntityManager.find(Post.class, post.getId());
        User fetchedAuthor = fetchedPost.getAuthor();
        assertNotNull(fetchedAuthor.getId());   // to confirm the author has been persisted
        assertEquals("yyy", fetchedAuthor.getUsername());
        assertEquals("yyy", fetchedAuthor.getFirstName());
        assertEquals("yyy", fetchedAuthor.getLastName());
    }
}