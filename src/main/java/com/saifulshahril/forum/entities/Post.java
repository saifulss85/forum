package com.saifulshahril.forum.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "posts")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String body;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    @NotNull
    private User author;

    @OneToMany(
            mappedBy = "post",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<CategoryPost> categoryPosts = new ArrayList<>();

    public void addCategory(Category category) {
        CategoryPost categoryPost = new CategoryPost(null, category, this);
        categoryPosts.add(categoryPost);
        category.getCategoryPosts().add(categoryPost);
    }

    public void removeCategory(Category category) {
        for (Iterator<CategoryPost> iterator = categoryPosts.iterator(); iterator.hasNext(); ) {
            CategoryPost categoryPost = iterator.next();

            if (categoryPost.getPost().equals(this) && categoryPost.getCategory().equals(category)) {
                iterator.remove();
                categoryPost.getCategory().getCategoryPosts().remove(categoryPost);
                categoryPost.setPost(null);
                categoryPost.setCategory(null);
            }
        }
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}