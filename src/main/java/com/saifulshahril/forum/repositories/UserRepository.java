package com.saifulshahril.forum.repositories;

import com.saifulshahril.forum.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
