package com.saifulshahril.forum.services.fakers;

import com.github.javafaker.Faker;
import com.github.javafaker.Superhero;
import com.saifulshahril.forum.entities.User;
import org.springframework.stereotype.Service;

import java.util.HashSet;

@Service("userFakerService")
public class UserFakerService implements SingleFaker<User> {
    @Override
    public User make() {
        Faker faker = new Faker();

        Superhero superhero = faker.superhero();

        return new User(
                null,
                superhero.name().toLowerCase(),
                superhero.name(),
                superhero.suffix(),
                new HashSet<>()
        );
    }
}
