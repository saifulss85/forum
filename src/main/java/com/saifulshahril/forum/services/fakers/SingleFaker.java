package com.saifulshahril.forum.services.fakers;

public interface SingleFaker<T> {
    T make();
}
