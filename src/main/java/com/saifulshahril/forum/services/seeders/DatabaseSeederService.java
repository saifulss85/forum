package com.saifulshahril.forum.services.seeders;

import com.saifulshahril.forum.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("databaseSeederService")
public class DatabaseSeederService implements Seeder {
    private final UserService userService;
    private final UserSeederService userSeederService;

    @Autowired
    public DatabaseSeederService(UserService userService, UserSeederService userSeederService) {
        this.userService = userService;
        this.userSeederService = userSeederService;
    }

    @Override
    public void run() {
        if (userService.count() == 0) userSeederService.run();
    }
}
