package com.saifulshahril.forum.services.seeders;

import com.saifulshahril.forum.entities.User;
import com.saifulshahril.forum.services.UserService;
import com.saifulshahril.forum.services.fakers.UserFakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userSeederService")
public class UserSeederService implements Seeder {
    private final UserFakerService userFakerService;
    private UserService userService;

    @Autowired
    public UserSeederService(UserFakerService userFakerService, UserService userService) {
        this.userFakerService = userFakerService;
        this.userService = userService;
    }

    @Override
    public void run() {
        int numRecords = 10;

        for (int i = 0; i < numRecords; i++) {
            User user = userFakerService.make();
            userService.create(user);
        }
    }
}
