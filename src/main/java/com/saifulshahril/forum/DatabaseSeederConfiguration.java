package com.saifulshahril.forum;

import com.saifulshahril.forum.services.seeders.DatabaseSeederService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

@Configuration
@Profile("default")
public class DatabaseSeederConfiguration {
    private final DatabaseSeederService databaseSeederService;
    private final Environment environment;

    @Autowired
    public DatabaseSeederConfiguration(DatabaseSeederService databaseSeederService, Environment environment) {
        this.databaseSeederService = databaseSeederService;
        this.environment = environment;
    }

    /**
     * Seeding should not happen in these profiles: prod, test
     * Seeding should only happen in: default
     */
    @EventListener(ApplicationReadyEvent.class)
    public void seedDatabaseIfNotSeeded() {
        String[] activeProfiles = environment.getActiveProfiles();

        // when multiple profiles are set
        if (activeProfiles.length > 1) return;

        // when only 1 profile is set but it's not "default"
        if (activeProfiles.length == 1 && !activeProfiles[0].equalsIgnoreCase("default")) return;

        // should get here only when there are 0 profiles set or it's 1 profile and that's "default"
        databaseSeederService.run();
    }
}
