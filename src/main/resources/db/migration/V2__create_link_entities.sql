/*! SET default_storage_engine=INNODB */;

CREATE TABLE category_post
(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  category_id bigint(20) NOT NULL,
  post_id bigint(20) NOT NULL,
  PRIMARY KEY (id)
);