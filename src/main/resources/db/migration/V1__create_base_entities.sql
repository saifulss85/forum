/*! SET default_storage_engine=INNODB */;

CREATE TABLE users
(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  username varchar(100) NOT NULL,
  first_name varchar(50) NOT NULL,
  last_name varchar(50) DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT uk_username UNIQUE (username)
);

CREATE TABLE posts
(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  title varchar(100) NOT NULL,
  body varchar(50) NOT NULL,
  author_id bigint(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE categories
(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  title varchar(100) NOT NULL,
  PRIMARY KEY (id)
);