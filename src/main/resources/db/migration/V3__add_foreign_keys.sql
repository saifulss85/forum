/*! SET default_storage_engine=INNODB */;

ALTER TABLE posts
  ADD CONSTRAINT posts_user_fk FOREIGN KEY (author_id) REFERENCES users (id);

ALTER TABLE category_post
  ADD CONSTRAINT category_category_post_fk FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE category_post
  ADD CONSTRAINT category_post_post_fk FOREIGN KEY (post_id) REFERENCES posts (id);